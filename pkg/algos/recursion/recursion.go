package recursion

func Fibonacci(n int) int {
	if n < 2 {
		return n
	}
	return Fibonacci(n-2) + Fibonacci(n-1)
}

/*// without recursion
func Fibonacci(n int) (val int) {
  if n != 0 {
    first := 0
    val = 1 //second
    for i := 1; i < n; i++ {
      temp := first
      first = val
      val = temp + first
    }
  }
  return val
}*/
