package recursion

import "testing"

func TestFibonacci(t *testing.T) {
	match := Fibonacci(2)
	if match != 1 {
		t.Errorf("fib == %d; expected 1", match)
	}
	match = Fibonacci(3)
	if match != 2 {
		t.Errorf("fib == %d; expected 2", match)
	}
	match = Fibonacci(7)
	if match != 13 {
		t.Errorf("fib == %d; expected 13", match)
	}
}
