package search

// search for value in a slice using linear search
func LinearSearch(lst []int, val int) bool {
	for _, n := range lst {
		if n == val {
			return true
		}
	}
	return false
}

// find the greatest valued number in a slice
// using o(n) instead of o(n^2)
func GetGreatestNumber(lst []int) (max int) {
	for _, n := range lst {
		if n > max {
			max = n
		}
	}
	return max
}

// search for a value in a slice using binary search
func BinarySearch(lst []int, val int) bool {
	low := 0
	high := len(lst) - 1
	for low <= high {
		middle := (high + low) / 2
		if lst[middle] == val {
			return true
		} else if lst[middle] > val {
			high = middle - 1
		} else if lst[middle] < val {
			low = middle + 1
		}
	}
	return false
}
