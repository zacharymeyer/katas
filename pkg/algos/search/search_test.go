package search

import "testing"

func TestLinearSearch(t *testing.T) {
	lst := []int{1, 2, 5, 13, 69, 420, 666, 777, 1337, 42069, 69420}

	got := LinearSearch(lst, 1)
	if got != true {
		t.Errorf("LinearSearch(lst, 1) = %t; expect true", got)
	}

	got = LinearSearch(lst, 69)
	if got != true {
		t.Errorf("LinearSearch(lst, 69) = %t; expect true", got)
	}

	got = LinearSearch(lst, 666)
	if got != true {
		t.Errorf("LinearSearch(lst, 1) = %t; expect true", got)
	}

	got = LinearSearch(lst, 667)
	if got != false {
		t.Errorf("LinearSearch(lst, 1) = %t; expect false", got)
	}

	got = LinearSearch(lst, 69421)
	if got != false {
		t.Errorf("LinearSearch(lst, 1) = %t; expect false", got)
	}
}

func TestGetGreatestNumber(t *testing.T) {
	lst := []int{10, 11, 1337, 59, 666, 777, 69, 555}
	n := GetGreatestNumber(lst)
	if n != 1337 {
		t.Errorf("GetGreatestNumber(lst) == %d; expected 1337", n)
	}
	lst2 := []int{10, 11, 1337, 59, 666, 42069, 69420, 777, 69, 555}
	n = GetGreatestNumber(lst2)
	if n != 69420 {
		t.Errorf("GetGreatestNumber(lst) == %d; expected 69420", n)
	}
}

func TestBinarySearch(t *testing.T) {
	lst := []int{1, 2, 5, 13, 69, 420, 666, 777, 1337, 42069, 69420}

	got := BinarySearch(lst, 1)
	if got != true {
		t.Errorf("BinarySearch(lst, 1) = %t; expect true", got)
	}

	got = BinarySearch(lst, 69)
	if got != true {
		t.Errorf("BinarySearch(lst, 69) = %t; expect true", got)
	}

	got = BinarySearch(lst, 666)
	if got != true {
		t.Errorf("BinarySearch(lst, 1) = %t; expect true", got)
	}

	got = BinarySearch(lst, 667)
	if got != false {
		t.Errorf("BinarySearch(lst, 1) = %t; expect false", got)
	}

	got = BinarySearch(lst, 69421)
	if got != false {
		t.Errorf("BinarySearch(lst, 1) = %t; expect false", got)
	}
}
