package line

import "testing"

func TestBresenham(t *testing.T) {
	pt1 := Point{0, 0}
	pt2 := Point{5, 5}

	correct := []Point{{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}}
	line := Bresenham(pt1, pt2)

	for i, pt := range line {
		if pt.X != correct[i].X && pt.Y != correct[i].Y {
			t.Errorf("%d pt == %d, %d; expected %d, %d", i, pt.X, pt.Y, correct[i].X, correct[i].Y)
		}
	}
}

func BenchmarkBresenham(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pt1 := Point{RandInt(1, 100), RandInt(1, 100)}
		pt2 := Point{RandInt(1, 100), RandInt(1, 100)}
		Bresenham(pt1, pt2)
	}
}

func TestDDA(t *testing.T) {
	pt1 := Point{0, 0}
	pt2 := Point{5, 5}

	correct := []Point{{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}}
	line := DDA(pt1, pt2)

	for i, pt := range line {
		if pt.X != correct[i].X && pt.Y != correct[i].Y {
			t.Errorf("%d pt == %d, %d; expected %d, %d", i, pt.X, pt.Y, correct[i].X, correct[i].Y)
		}
	}
}

func BenchmarkDDA(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pt1 := Point{RandInt(1, 50), RandInt(1, 50)}
		pt2 := Point{RandInt(50, 100), RandInt(50, 100)}
		DDA(pt1, pt2)
	}
}
