package line

import (
	"math"
	"math/rand"
	"time"
)

type Point struct {
	X, Y int
}

func RandInt(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min - rand.Intn(max-min)
}

func Bresenham(pt1, pt2 Point) (line []Point) {
	m := 2 * (pt2.Y - pt1.Y)
	slope := m - (pt2.X - pt1.X)
	y := pt1.Y
	for x := pt1.X; x <= pt2.X; x++ {
		line = append(line, Point{x, y})
		slope += m
		if slope >= 0 {
			y++
			slope -= 2 * (pt2.X - pt1.X)
		}
	}
	return line
}

// pt2 must be >= pt1
func DDA(pt1, pt2 Point) (line []Point) {
	dx := pt2.X - pt1.X
	dy := pt2.Y - pt1.Y
	steps := int(math.Abs(float64(dy)))

	if math.Abs(float64(dx)) > math.Abs(float64(dy)) {
		steps = int(math.Abs(float64(dx)))
	}

	dx /= steps
	dy /= steps
	x := pt1.X
	y := pt1.Y

	for i := 0; i <= steps; i++ {
		line = append(line, Point{x, y})
		x += dx
		y += dy
	}

	return line
}
