package sort

import "fmt"

func BubbleSort(lst []int) []int {
	length := len(lst) - 1
	pass := true
	for pass {
		pass = false
		for i := 0; i < length; i++ {
			curr := lst[i]
			next := lst[i+1]
			if curr > next {
				lst[i] = next
				lst[i+1] = curr
				pass = true
			}
		}
		length--
	}
	return lst
}

func SelectionSort(lst []int) []int {
	length := len(lst)
	for i := 0; i < length-1; i++ {
		lowest := i
		for j := i + 1; j < length; j++ {
			if lst[lowest] > lst[j] {
				lowest = j
			}
		}
		if lowest != i {
			lst[i] += lst[lowest]
			lst[lowest] = lst[i] - lst[lowest]
			lst[i] -= lst[lowest]
		}
	}
	return lst
}

func InsertionSort(lst []int) []int {
	for i := 1; i < len(lst); i++ {
		tmp := lst[i]
		pos := i - 1
		for pos >= 0 {
			if lst[pos] > tmp {
				lst[pos+1] = lst[pos]
				pos--
			} else {
				break
			}
		}
		lst[pos+1] = tmp
	}
	return lst
}

type Array struct {
	array []int
}

func NewArray(array []int) *Array {
	return &Array{array: array}
}

func (a *Array) partition(left, right int) int {
	// pivot is right-most element of the array
	pivot_idx := right
	// pivot value
	pivot := a.array[pivot_idx]
	// right ptr should be to the left of the pivot
	right--
	for {
		for a.array[left] < pivot {
			left++
		}
		for a.array[right] > pivot {
			right--
		}
		// check if left has reached or went passed the right
		if left >= right {
			break
		}
		// swap values of left and right ptrs
		a.array[left], a.array[right] = a.array[right], a.array[left]
		left++
	}
	// swap left value with pivot
	a.array[left], a.array[pivot_idx] = a.array[pivot_idx], a.array[left]
	return left
}

func (a *Array) sort(left, right int) {
	// 0 or 1 == base case
	if right-left <= 0 {
		return
	}
	pivot_idx := a.partition(left, right)
	a.sort(left, pivot_idx-1)
	a.sort(pivot_idx+1, right)
}

func (a *Array) Array() []int {
	return a.array
}

func QuickSort(array []int) []int {
	a := NewArray(array)
	a.sort(0, len(array)-1)

	fmt.Println(a.Array())
	return a.Array()
}
