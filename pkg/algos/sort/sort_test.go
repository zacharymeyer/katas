package sort

import (
	"testing"
)

func TestBubbleSort(t *testing.T) {
	correct := []int{1, 3, 4, 7, 9, 99, 420}
	lst := []int{1, 3, 9, 7, 420, 4, 99}
	match := BubbleSort(lst)
	if len(match) != len(correct) {
		t.Errorf("len(BubbleSort(lst)) = %d; expected %d", len(match), len(correct))
	}
	for i := 0; i < len(match); i++ {
		if match[i] != correct[i] {
			t.Errorf("match[%d] = %d; expected = %d", i, match[i], correct[i])
		}
	}
}

func TestSelectionSort(t *testing.T) {
	correct := []int{1, 3, 4, 7, 9, 99, 420}
	lst := []int{1, 3, 9, 7, 420, 4, 99}
	match := SelectionSort(lst)
	if len(match) != len(correct) {
		t.Errorf("len(SelectionSort(lst)) = %d; expected %d", len(match), len(correct))
	}
	for i := 0; i < len(match); i++ {
		if match[i] != correct[i] {
			t.Errorf("match[%d] = %d; expected = %d", i, match[i], correct[i])
		}
	}
}

func TestInsertionSort(t *testing.T) {
	correct := []int{1, 3, 4, 7, 9, 99, 420}
	lst := []int{1, 3, 9, 7, 420, 4, 99}
	match := InsertionSort(lst)
	if len(match) != len(correct) {
		t.Errorf("len(InsertionSort(lst)) = %d; expected %d", len(match), len(correct))
	}
	for i := 0; i < len(match); i++ {
		if match[i] != correct[i] {
			t.Errorf("match[%d] = %d; expected = %d", i, match[i], correct[i])
		}
	}
}

func TestQuickSort(t *testing.T) {
	correct := []int{1, 4, 7, 9, 10, 99, 420}
	lst := []int{1, 10, 9, 7, 420, 4, 99}
	match := QuickSort(lst)
	if len(match) != len(correct) {
		t.Errorf("len(QuickSort(lst)) = %d; expected %d", len(match), len(correct))
	}
	for i := 0; i < len(match); i++ {
		if match[i] != correct[i] {
			t.Errorf("match[%d] = %d; expected = %d", i, match[i], correct[i])
		}
	}
}
