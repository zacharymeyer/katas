package hashtable

import "errors"

type (
	HashTable struct {
		size int
		vals []*Value
	}

	Value struct {
		key string
		val int
	}
)

func NewHashTable() *HashTable {
	return &HashTable{
		size: 0,
		vals: make([]*Value, 127),
	}
}

func (h *HashTable) hash(key string) (hash int) {
	for _, v := range key {
		hash += int(v)
	}
	return hash % len(h.vals)
}

func (h *HashTable) Size() int {
	return h.size
}

func (h *HashTable) Set(key string, val int) {
	idx := h.hash(key)
	h.vals[idx] = &Value{key, val}
	h.size++
}

func (h *HashTable) Get(key string) (int, error) {
	idx := h.hash(key)
	if h.vals[idx] != nil {
		return h.vals[idx].val, nil
	}
	return -1, errors.New("error getting key")
}

func (h *HashTable) Remove(key string) bool {
	idx := h.hash(key)
	if h.vals[idx] != nil {
		h.vals[idx] = nil
		h.size--
		return true
	}
	return false
}
