package hashtable

import "testing"

func TestHashTable(t *testing.T) {
	ht := NewHashTable()
	if ht == nil {
		t.Errorf("ht == nil")
	}
	if ht.Size() != 0 {
		t.Errorf("len(ht.val) == %d; expected 0", ht.Size())
	}
}

func TestHash(t *testing.T) {
	ht := NewHashTable()
	hash := ht.hash("hello")
	if hash != 24 {
		t.Errorf("hash == %d; expected 24", hash)
	}
}

func TestSetHashTable(t *testing.T) {
	ht := NewHashTable()
	ht.Set("test", 1)
	if ht.Size() != 1 {
		t.Errorf("ht.Size() == %d; expected 1", ht.Size())
	}
	ht.Set("testi", 69)
	if ht.Size() != 2 {
		t.Errorf("ht.Size() == %d; expected 2", ht.Size())
	}
	ht.Set("hello", 420)
	if ht.Size() != 3 {
		t.Errorf("ht.Size() == %d; expected 3", ht.Size())
	}
}

func TestGetHashTable(t *testing.T) {
	ht := NewHashTable()
	ht.Set("test", 1)
	ht.Set("hello", 69)
	ht.Set("hehe", 420)
	val, _ := ht.Get("test")
	if val != 1 {
		t.Errorf("val == %d; expected 1", val)
	}
	val, _ = ht.Get("hello")
	if val != 69 {
		t.Errorf("val == %d; expected 69", val)
	}
	val, _ = ht.Get("hehe")
	if val != 420 {
		t.Errorf("val == %d; expected 420", val)
	}
}

func TestRemoveHashTable(t *testing.T) {
	ht := NewHashTable()
	ht.Set("test", 1)
	ht.Set("hello", 69)
	ht.Set("hehe", 420)
	ht.Set("heha", 1337)
	ht.Set("todo", 42069)
	ht.Set("omg", 99)
	if ht.Size() != 6 {
		t.Errorf("size == %d; expected 6", ht.Size())
	}

	testVals := []string{"test", "hello", "hehe", "heha", "todo", "omg"}
	for i, v := range testVals {
		ht.Remove(v)
		v, err := ht.Get(v)
		if err == nil {
			t.Errorf("err should be nil; v == %d, err == %s", v, err)
		}
		if ht.Size() != len(testVals)-i-1 {
			t.Errorf("loop %d; size == %d; expected %d", i, ht.Size(), len(testVals)-i-1)
		}
	}
}
