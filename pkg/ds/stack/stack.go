package stack

import "errors"

type Stack struct {
	val []int
}

func NewStack() *Stack {
	return &Stack{}
}

func (s *Stack) push(val int) {
	s.val = append(s.val, val)
}

func (s *Stack) pop() (val int, err error) {
	if len(s.val) > 0 {
		val = s.val[len(s.val)-1]
		s.val = s.val[:len(s.val)-1]
	} else {
		err = errors.New("error popping from stack")
	}
	return val, err
}

func (s *Stack) peek() (val int, err error) {
	if len(s.val) > 0 {
		val = s.val[len(s.val)-1]
	} else {
		err = errors.New("error peeking stack")
	}
	return val, err
}
