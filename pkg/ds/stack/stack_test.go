package stack

import "testing"

func TestNewStack(t *testing.T) {
	s := NewStack()
	if s == nil {
		t.Errorf("s is nil")
	}
}

func TestPushStack(t *testing.T) {
	s := NewStack()
	if len(s.val) != 0 {
		t.Errorf("len(s.val) == %d; expected 0", len(s.val))
	}
	s.push(1)
	if len(s.val) != 1 {
		t.Errorf("len(s.val) == %d; expected 1", len(s.val))
	}
	s.push(69)
	if len(s.val) != 2 {
		t.Errorf("len(s.val) == %d; expected 2", len(s.val))
	}
}

func TestPopStack(t *testing.T) {
	s := NewStack()
	s.push(1)
	s.push(69)
	s.push(420)

	testVals := []int{420, 69, 1}
	for i := 0; i < len(s.val); i++ {
		val, err := s.pop()
		if err != nil {
			t.Errorf("s.pop(): %s", err)
		}
		if testVals[i] != val {
			t.Errorf("val == %d; expected %d", val, testVals[i])
		}
	}
}

func TestPeekStack(t *testing.T) {
	s := NewStack()
	s.push(1)
	s.push(69)
	s.push(420)

	val, _ := s.peek()
	if val != 420 {
		t.Errorf("val == %d; expected 420", val)
	}
}
