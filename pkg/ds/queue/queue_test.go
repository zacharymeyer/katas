package queue

import "testing"

func TestQueue(t *testing.T) {
	q := NewQueue()
	if q == nil {
		t.Errorf("q == nil")
	}
}

func TestPushQueue(t *testing.T) {
	q := NewQueue()
	if len(q.val) != 0 {
		t.Errorf("len(q.val) == %d; expected 0", len(q.val))
	}
	q.push(1)
	if len(q.val) != 1 {
		t.Errorf("len(q.val) == %d; expected 1", len(q.val))
	}
	q.push(69)
	if len(q.val) != 2 {
		t.Errorf("len(q.val) == %d; expected 2", len(q.val))
	}
}

func TestPopQueue(t *testing.T) {
	q := NewQueue()
	q.push(1)
	q.push(69)
	q.push(420)
	q.push(1337)

	testVals := []int{1, 69, 420, 1337}
	for i := 0; i < len(q.val); i++ {
		val, err := q.pop()
		if err != nil {
			t.Errorf("q.pop(): %s", err)
		}
		if testVals[i] != val {
			t.Errorf("val == %d; expected %d", val, testVals[i])
		}
	}
}

func TestPeekQueue(t *testing.T) {
	q := NewQueue()
	q.push(1)
	q.push(69)
	q.push(420)

	val, _ := q.peek()
	if val != 1 {
		t.Errorf("val == %d; expected 1", val)
	}
}
