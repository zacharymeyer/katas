package queue

import "errors"

type Queue struct {
	val []int
}

func NewQueue() *Queue {
	return &Queue{}
}

func (q *Queue) push(val int) {
	q.val = append(q.val, val)
}

func (q *Queue) pop() (val int, err error) {
	if len(q.val) > 0 {
		val = q.val[0]
		q.val = q.val[1:]
	} else {
		err = errors.New("error occurred popping from queue")
	}
	return val, err
}

func (q *Queue) peek() (val int, err error) {
	if len(q.val) > 0 {
		val = q.val[0]
	} else {
		err = errors.New("error occurred peeking queue")
	}
	return val, err
}
