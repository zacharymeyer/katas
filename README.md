# katas

[![pipeline status](https://gitlab.com/zacharymeyer/katas/badges/main/pipeline.svg)](https://gitlab.com/zacharymeyer/katas/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

This repo is strictly for me to practice various DS&A. These are my implementations of the following algorithms and data structures following along with a few books.

### Supported Algos

- [Bubble sort](https://en.wikipedia.org/wiki/Bubble_sort)
- [Selection sort](https://en.wikipedia.org/wiki/Selection_sort)
- [Insertion sort](https://en.wikipedia.org/wiki/Insertion_sort)
- [Quick sort](https://en.wikipedia.org/wiki/Quicksort)
- [Linear search](https://en.wikipedia.org/wiki/Linear_search)
- [Binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm)

### Supported Data Structs

- [Stack](<https://en.wikipedia.org/wiki/Stack_(abstract_data_type)>)
- [Queue](<https://en.wikipedia.org/wiki/Queue_(abstract_data_type)>)
- [Hashtable](https://en.wikipedia.org/wiki/Hash_table)

### Credits / Books

- [A Common-Sense Guide to Data Structures and Algorithms - Jay Wengrow](https://www.amazon.com/Common-Sense-Guide-Data-Structures-Algorithms/dp/1680502441)
- [Data Structures and Algorithms Made Easy - Narasimha Karumanchi](https://www.amazon.com/Data-Structures-Algorithms-Made-Easy/dp/819324527X)
- [Introduction to Algorithms - Thomas H. Cormen](https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844)

### Goals / TODO

- CLI tool to allow me to generate tests daily to reimplement algorithms. (Inspired by [ThePrimeagen](https://www.twitch.tv/theprimeagen).)
- Some other stuff
- Support m > 1 & m < 1 for line algorithms
