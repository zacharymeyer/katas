package main

import (
	"fmt"

	"gitlab.com/zacharymeyer/katas/pkg/algos/search"
)

func main() {
	fmt.Println("Hello, World!")
	lst := []int{1, 2, 5, 13, 69, 420, 666, 777, 1337, 42069, 69420}
	_ = search.BinarySearch(lst, 420)
}
